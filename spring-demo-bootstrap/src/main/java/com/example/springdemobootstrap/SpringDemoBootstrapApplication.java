package com.example.springdemobootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemoBootstrapApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoBootstrapApplication.class, args);
    }

}
