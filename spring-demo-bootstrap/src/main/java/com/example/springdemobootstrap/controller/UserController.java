package com.example.springdemobootstrap.controller;

import com.example.springdemobootstrap.entity.User;
import com.example.springdemobootstrap.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;

   @RequestMapping(path = "/")
    public String index(Model model){
        List<User> list = (List<User>) userRepository.findAll();
        //step 3 return to view
        model.addAttribute("list",list);
        return "index";
    }
    @RequestMapping(path = "/add")
    public String addUser(Model model){
       model.addAttribute("user", new User());
       return "addUser";
    }
    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public String saveUser(User user){
       userRepository.save(user);
       return "redirect:/";
    }
    @RequestMapping(path = "/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam("id") Integer Id,Model model){
        Optional<User> optionalUser = userRepository.findById(Id);
        optionalUser.ifPresent(user -> model.addAttribute("user",user));
        return "editUser";
    }
    @RequestMapping(path = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") Integer id,Model model){
       userRepository.deleteById(id);
       return "redirect:/";
    }
    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public String searchByName(@RequestParam("name") String name,Model model){
       List<User> search = userRepository.findbyName(name);
       model.addAttribute("search",search);
        return "index";
    }
}
