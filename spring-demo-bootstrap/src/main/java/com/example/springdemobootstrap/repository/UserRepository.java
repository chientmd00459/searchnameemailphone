package com.example.springdemobootstrap.repository;

import com.example.springdemobootstrap.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
    @Query("Select c from User c where c.name LIKE  %?1% "+" or c.phone LIKE %?1% "+" or c.email LIKE %?1%")
    List<User> findbyName(String name);
}
